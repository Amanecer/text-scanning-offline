package com.realityglass.tso;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static java.lang.System.lineSeparator;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TextScannerTest {
    @Test
    public void testEmpty() throws IOException {
        assertResult("empty");
    }

    @Test
    public void testShortLine() {
        assertThrows(TextScannerException.class, () -> readResult("shortLine"));
    }

    @Test
    public void testSingleChunk() throws IOException {
        assertResult("singleChunk", "000000000");
    }

    @Test
    public void testIncompleteSingleChunk() {
        assertThrows(TextScannerException.class, () -> readResult("incompleteSingleChunk"));
    }

    @Test
    public void testMultipleChunks() throws IOException {
        assertResult("multipleChunks", "123456789", "123456789", "123456789");
    }

    @Test
    public void testIncompleteMultipleChunks() {
        assertThrows(TextScannerException.class, () -> readResult("incompleteMultipleChunks"));
    }

    @Test
    public void testMultipleChunksWithoutEmptyLine() {
        assertThrows(TextScannerException.class, () -> readResult("multipleChunksWithoutEmptyLine"));
    }

    @Test
    public void testMultipleChunksWithIllegalRow() throws IOException {
        assertResult("multipleChunksWithIllegalRow", "123456789", "123456?89 ILL", "123456789");
    }

    public String readResult(String path) throws IOException {
        StringBuilder sb = new StringBuilder();
        TextScanner.readAllNumbers(
                getClass().getClassLoader().getResourceAsStream(path),
                s -> sb.append(s).append(lineSeparator()));
        return sb.toString();
    }

    public void assertResult(String path, String... expected) throws IOException {
        StringBuilder sb = new StringBuilder();
        for (String str : expected) {
            sb.append(str).append(lineSeparator());
        }
        assertEquals(sb.toString(), readResult(path));
    }
}