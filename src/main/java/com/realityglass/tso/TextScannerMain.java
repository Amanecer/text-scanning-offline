package com.realityglass.tso;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Runs the text scanner. Multiple file names can be specified as java args.
 */
public class TextScannerMain {
    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("No files to process");
            return;
        }

        for (String path : args) {
            System.out.println("Processing file " + path);
            TextScanner.readAllNumbers(new FileInputStream(path), System.out::println);
        }
    }
}
