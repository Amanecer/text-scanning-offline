package com.realityglass.tso;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * The class implements the main text scanner functionality.
 * See README.md for more details on the original requirements.
 * Here are some examples how the class can be used:
 *
 * <blockquote><pre>
 *  TextScanner.readAllNumbers(inputStream, consumer);
 * </pre></blockquote>
 * or
 * <blockquote><pre>
 *  try (TextScanner reader = new TextScanner(inputStream)) {
 *      reader.readAllNumbers(consumer);
 *  }
 * </pre></blockquote><p>
 * The class can be easily modified to support custom symbol maps and custom sizes.
 * This has not been implemented because it is not part of the original requirements.
 */
public class TextScanner implements Closeable {
    /**
     * The suffix to add after numbers with invalid symbols
     */
    private static final String INVALID_SUFFIX = " ILL";

    /**
     * The character to print for an invalid symbol
     */
    private static final char INVALID_CHAR = '?';

    /**
     * The raw symbol width
     */
    private static final int SYMBOL_WIDTH = 3;

    /**
     * The raw symbol height
     */
    private static final int SYMBOL_HEIGHT = 3;

    /**
     * The expected number of symbols per line
     */
    private static final int SYMBOLS_PER_LINE = 9;

    /**
     * The mapping between the raw symbols and the characters.
     * The map is initialised statically to minimise the TextScanner creation time.
     * Alternatively this can be done in the constructor if more flexibility is required.
     */
    private static final Map<String, Character> symbolMap = createDefaultSymbolMap();


    /**
     * The reader to read the data from
     */
    private final LineNumberReader reader;

    /**
     * Constructs the text scanner with the default settings and the input stream to read from.
     *
     * @param in The input stream to read the data from
     */
    public TextScanner(InputStream in) {
        this.reader = new LineNumberReader(new InputStreamReader(in));
    }

    /**
     * Creates the default symbols map.
     *
     * @return The default symbol map
     */
    private static Map<String, Character> createDefaultSymbolMap() {
        final String chars = "0123456789";
        final String[] chunk = {
                " _     _  _     _  _  _  _  _ ",
                "| |  | _| _||_||_ |_   ||_||_|",
                "|_|  ||_  _|  | _||_|  ||_| _|"
        };
        return createSymbolMap(chars, chunk);
    }

    /**
     * Creates a symbol map from chars and a chunk. This method could be used in the future if support for custom symbols is required.
     *
     * @param chars The string containing all supported characters/digits
     * @param chunk The chunk containing all corresponding unparsed symbols
     * @return The symbol map constructed from the chars and the chunk
     */
    @SuppressWarnings("SameParameterValue")
    private static Map<String, Character> createSymbolMap(String chars, String[] chunk) {
        String[] rawSymbols = extractRawSymbols(chunk, chars.length());
        Map<String, Character> symbolMap = new HashMap<>();
        for (int i = 0; i < chars.length(); i++) {
            char ch = chars.charAt(i);
            symbolMap.put(rawSymbols[i], ch);
        }
        return symbolMap;
    }

    /**
     * Reads all numbers from the underlying stream and pass them to the provided consumer.
     *
     * @param consumer The consumer to call
     * @throws IOException If failed to read the numbers
     */
    public void readAllNumbers(Consumer<String> consumer) throws IOException {
        String[] chunk;
        while ((chunk = readChunk()) != null) {
            String[] rawSymbols = extractRawSymbols(chunk, SYMBOLS_PER_LINE);
            String number = translateRawSymbols(rawSymbols);
            consumer.accept(number);
            skipEmptyLine();
        }
    }

    /**
     * Reads all numbers from the underlying stream with the default symbol map and pass them to the provided consumer.
     *
     * @param consumer The consumer to call
     * @throws IOException If failed to read the numbers
     */
    public static void readAllNumbers(InputStream in, Consumer<String> consumer) throws IOException {
        try (TextScanner reader = new TextScanner(in)) {
            reader.readAllNumbers(consumer);
        }
    }

    /**
     * Skips the next line and validates that it is empty.
     *
     * @throws TextScannerException If the next line is not empty
     */
    private void skipEmptyLine() throws IOException {
        String line = reader.readLine();
        if (line == null) {
            //Nothing else to read
            return;
        }
        if (!line.isEmpty()) {
            throw new TextScannerException("Expected an empty line at line " + reader.getLineNumber());
        }
    }

    /**
     * Reads the next chunk with the unparsed raw symbols.
     *
     * @return A string array with an unparsed raw symbols or null if the end of the stream is reached
     * @throws IOException If failed to read the next chunk
     */
    private String[] readChunk() throws IOException {
        final int expectedLineLength = SYMBOL_WIDTH * SYMBOLS_PER_LINE;
        String[] chunk = new String[SYMBOL_HEIGHT];
        for (int i = 0; i < SYMBOL_HEIGHT; i++) {
            String line = reader.readLine();
            if (line == null) {
                if (i == 0) {
                    //Nothing to read
                    return null;
                }
                throw new TextScannerException("Failed to read the next chunk: unexpected end of stream");
            }
            if (line.length() != expectedLineLength) {
                throw new TextScannerException("Incorrect line length at line " + reader.getLineNumber() + ": expected=" + expectedLineLength + ", actual=" + line.length());
            }
            chunk[i] = line;
        }
        return chunk;
    }

    /**
     * Extracts the raw symbols from a chunk. Each raw symbol consists of several lines concatenated together.
     *
     * @param chunk               The chunk to extract the symbols from
     * @param expectedSymbolCount How many symbols we expect to read
     * @return The array of raw symbol strings
     */
    private static String[] extractRawSymbols(String[] chunk, int expectedSymbolCount) {
        String[] rawSymbols = new String[expectedSymbolCount];
        for (int posSymbol = 0; posSymbol < expectedSymbolCount; posSymbol++) {
            StringBuilder sb = new StringBuilder();
            for (int posLine = 0; posLine < SYMBOL_HEIGHT; posLine++) {
                sb.append(chunk[posLine], posSymbol * SYMBOL_WIDTH, (posSymbol + 1) * SYMBOL_WIDTH);
            }
            rawSymbols[posSymbol] = sb.toString();
        }
        return rawSymbols;
    }

    /**
     * Translates raw symbols into their corresponding characters.
     *
     * @param rawSymbols The raw symbols to translate
     * @return The string with translated characters
     */
    private String translateRawSymbols(String[] rawSymbols) {
        StringBuilder result = new StringBuilder();
        boolean hasInvalidSymbols = false;
        for (int i = 0; i < SYMBOLS_PER_LINE; i++) {
            Character symbol = symbolMap.get(rawSymbols[i]);
            if (symbol == null) {
                hasInvalidSymbols = true;
                result.append(INVALID_CHAR);
            } else {
                result.append(symbol);
            }
        }

        if (hasInvalidSymbols) {
            //I assume here that "ILL" has to be appended after each incorrect number
            result.append(INVALID_SUFFIX);
        }

        return result.toString();
    }

    /**
     * Closes this text scanner.
     *
     * @throws IOException If an I/O error occurs
     */
    @Override
    public void close() throws IOException {
        reader.close();
    }
}
