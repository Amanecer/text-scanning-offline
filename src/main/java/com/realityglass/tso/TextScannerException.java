package com.realityglass.tso;

import java.io.IOException;

public class TextScannerException extends IOException {
    public TextScannerException(String message) {
        super(message);
    }
}
